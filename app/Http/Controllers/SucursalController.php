<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SucursalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        return \Inertia\Inertia::render('sucursal',[
            'sucursales'=>\App\Models\Sucursal::all(),
            'rubros'=>\App\Models\Local::all(),
        ]);
    }      
    public function get(){
        $sucursal = \App\Models\Sucursal::with('rubro')->get();
        return response()->json($sucursal);
    }
    public function getRubros(){
        $Local = \App\Models\Local::all();
        return response()->json($Local);
    }

    public function activar(Request $r){

        $sucursal = \App\Models\sucursal::find($r->id);
        $sucursal->activo = $r->activo;
        $sucursal->save();
        return response()->json($sucursal->activado);
    }

    public function active2(Request $r){
       
       
        dd(1);
        return response()->json( $sucursal );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r){

        $sucursal = $r->id ? \App\Models\Sucursal::find($r->id) : new \App\Models\Sucursal;
        $sucursal->nombre    = $r->nombre;
        $sucursal->local_id  = $r->local_id;
        $sucursal->direccion = $r->direccion;
        $sucursal->activado  = true;
        $sucursal->save();


        return $this->get();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
