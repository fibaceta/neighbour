<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\perfil;

use Illuminate\Support\Facades\Auth;

class perfilController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perfils= perfil::All();

        //dd($perfils);
        return $perfils;
    }

    public function list(){

        $perfils= perfil::get();

        //dd($users);
        return $perfils;

    }

    public function desactivar($id){

        $perfil = perfil::find($id);
        $perfil->activado = false;
        $perfil->save();
        return perfil::get();


    }


    public function activar($id){

        $perfil = perfil::find($id);
        $perfil->activado = true;
        $perfil->save();
        return perfil::get();


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->id){
            $perfil = perfil::find($request->id);
        }else{
            $perfil =new perfil();
        }
        
        $perfil->nombre = $request->nombre;
        $perfil->descripcion=$request->descripcion;
        $perfil->activado = $request->activado;
        $perfil->save();

        return perfil::get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
