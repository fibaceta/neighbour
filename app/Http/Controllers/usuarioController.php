<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\perfil;
use App\Models\userPerfil;
use App\Models\usuarioLocalSucursal;
use Inertia\Inertia;
use Auth;


class usuarioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = User::with(['userPerfil.perfil'])->get();

        //$user->usuarioLocalSucursal->load(['local'. 'sucursal']);

        //dd($user);

        return $user;

    }

    public function permisos()
    {

        //$permisos =userPerfil::get();

        $user=Auth::user()->isPerfilSUadmin();

        //dd($user);
        //return response()->json($permisos);
        return Auth::user()->isPerfilSUadmin();

    }

    public function acceso($id)
    {

        $user=Auth::user()->id;

        $permisos =userPerfil::where('perfil_id',$id)->where('user_id', $user)->first();

        

        if ($permisos === null) {
            return false;
         }else{
             return true;
         }
    }

    public function desactivar($id){

        $user = User::find($id);
        $user->activado = false;
        $user->save();
        return User::with('userPerfil.perfil')->get();


    }

    public function perfil(){

        return perfil::all();


    }

    public function list(){

        $users= User::with(['userPerfil.perfil'])->with('usuarioLocalSucursal', function($query){
            $query->with(['local','sucursal']);
        })->get();

        //dd($users);
        return $users;

    }

    public function activar($id){
        $user = User::find($id);
        $user->activado = true;
        $user->save();
        return User::with('userPerfil.perfil')->get();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if($request->id){
            $usuario = User::find($request->id);
        }else{
            $usuario =new User();
        }
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->activado = $request->activado;
        $usuario->save();

        //$usuario_perfil = userPerfil::where('user_id', $id)->first();
        //$usuario_perfil->perfil_id = $request->perfil_id;
        //$usuario_perfil->save();

        return User::with('userPerfil.perfil')->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
