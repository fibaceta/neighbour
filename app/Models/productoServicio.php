<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductoServicio extends Model{
    //
    protected $table = "producto_servicios";
    protected $primaryKey = "id";
    protected $dates = ["created_at"];

    public  function tipo(){
        return $this->hasOne( \App\Models\TipoProductoServicio::class,'id','tipo_producto_servicio_id');
    }

}
