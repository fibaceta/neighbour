<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\UsuarioScope;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class Sucursal extends Model
{
    
    protected $table = "sucursals";
    protected $dates = ["created_at"];
    protected $primaryKey = "id";

    protected static function booted()
    {

        static::addGlobalScope('user', function (Builder $builder) {
            $builder->join('usuario_local_sucursals','sucursals.id','usuario_local_sucursals.sucursal_id');
            $builder->where('usuario_local_sucursals.user_id', '=', Auth::user()->id );
        });
    }
    public  function rubro(){
        return $this->hasOne( \App\Models\Local::class,'id','local_id');
    }


}
