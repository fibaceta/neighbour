<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoProductoServicio extends Model
{
    protected $table = "tipo_producto_servicios";
    protected $primaryKey = "id";

}
