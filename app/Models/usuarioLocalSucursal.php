<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class usuarioLocalSucursal extends Model
{
    //

    public function Local()
    {
        return $this->belongsTo(Local::class);
    }

    public function Sucursal()
    {
        return $this->belongsTo(Sucursal::class);
    }
}
