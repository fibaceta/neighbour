<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioLocalSucursalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_local_sucursals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned(); 
            $table->bigInteger('local_id')->unsigned(); 
            $table->bigInteger('sucursal_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->constrained();
            $table->foreign('local_id')->references('id')->on('locals')->constrained();
            $table->foreign('sucursal_id')->references('id')->on('sucursals')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_local_sucursals');
    }
}
