<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudProductoServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_producto_servicios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('solicitud_id')->unsigned();
            $table->bigInteger('producto_servicio_id')->unsigned();
            $table->timestamps();
            $table->foreign('solicitud_id')->references('id')->on('solicituds')->constrained();
            $table->foreign('producto_servicio_id')->references('id')->on('producto_servicios')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_producto_servicios');
    }
}
