
<div class="font-sans text-gray-900 antialiased">
    <div class=" flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
        <div id="panel_login" class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
            <form method="POST" action="/login">
                <input type="hidden" name="_token" :value="csrf" >
                <div>
                    <label for="email" value="Email" />Email
                    <jet-input id="email"  type="email" name="email" class="block h-12 w-auto" />
                </div>
                
                <div class="mt-4">
                    <label for="password" value="Clave" />Clave
                    <jet-input id="password"  class="block mt-1 w-full form-control" type="password" name="password" required autocomplete="current-password" />
                </div>

                <div class="block mt-4">
                    <label for="remember_me" class="flex items-center">
                        <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                        <span class="ml-2 text-sm text-gray-600">Recordar</span>
                    </label>
                </div>

                <div class="flex items-center justify-end mt-4">
                    <button class="ml-4 btn btn-blue">
                        Ingresar
                    </button>
                </div>

            </form>
        </div>
    </div>

</div> 