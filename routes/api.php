<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/servicio/active', function (Request $r) {
    $servicios = \App\Models\ProductoServicio::find($r->id);
    $servicios->activo = $r->activo;
    $servicios->save();
    return response()->json($local->activado);
});
Route::post('/servicio/save', function (Request $r) {
    $servicios = $r->id ? \App\Models\ProductoServicio::find($r->id) : new \App\Models\ProductoServicio;
    $servicios->tipo_producto_servicio_id = $r->tipo_producto_servicio_id;
    $servicios->nombre      = $r->nombre;
    $servicios->descripcion = $r->descripcion;
    $servicios->valor       = $r->valor;
    $servicios->duracion    = $r->duracion;
    $servicios->imagen      = $r->imagen;
    $servicios->activo      = 1;
    $servicios->save();

    $servicios = \App\Models\ProductoServicio::with('tipo')->get()->toArray();
    return json_encode($servicios);
    return true;
});
Route::get('/servicio/{id}', function ($id) {
    $servicios = \App\Models\ProductoServicio::with('tipo')->where('tipo_producto_servicio_id',$id)->get()->toArray();
    return json_encode($servicios);
    return Inertia\Inertia::render('servicios');
});



Route::get('/locales', function () {
    $locales = \App\Models\Local::get()->toArray();
    return json_encode($locales);
    return Inertia\Inertia::render('servicios');
});
Route::post('/local/active', function (Request $r) {
    $local = \App\Models\Local::find($r->id);
    $local->activado = $r->activado;
    $local->save();
    return response()->json($local->activado);
});

Route::post('/locales/save', function (Request $r) {

    $locales = $r->id ? \App\Models\Local::find($r->id) : new \App\Models\Local;
    $locales->nombre     = $r->nombre;
    $locales->activado   = $r->activado;
    $locales->created_at = $r->created_at;
    $locales->save();
    $locales = \App\Models\Local::get()->toArray();
    return json_encode($locales);
    
    return true;
});
