<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\usuarioController;
use App\Http\Controllers\sucursalController;
use App\Http\Controllers\perfilController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/locales', function () {
    return Inertia\Inertia::render('locales');
})->name('locales');

Route::get('/servicios', function () {
    return Inertia\Inertia::render('servicios');
})->name('servicios');
    Route::get('/', function () {
    return Inertia\Inertia::render('Welcome');
    return Inertia\Inertia::render('/../Jetstream/Welcome');
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');


Route::get('/usuarios', function () {
    return Inertia\Inertia::render('Usuario');
})->name('usuarios');

Route::get('/perfiles', function () {
    return Inertia\Inertia::render('Perfil');
})->name('perfiles');

Route::get('/agenda', function () {
    return Inertia\Inertia::render('Agenda');
})->name('agenda');


//Route::resource('/usuario', 'usuarioController');

Route::middleware(['auth:sanctum', 'verified'])->get('/usuario', [usuarioController::class, 'index'])->name('usuario');
Route::middleware(['auth:sanctum', 'verified'])->get('/usuario/lista', [usuarioController::class, 'list'])->name('usuario.lista');
Route::middleware(['auth:sanctum', 'verified'])->post('/usuario/desactivar/{id}', [usuarioController::class, 'desactivar'])->name('desactivar');
Route::middleware(['auth:sanctum', 'verified'])->post('/usuario/activar/{id}', [usuarioController::class, 'activar'])->name('activar');

Route::middleware(['auth:sanctum', 'verified'])->post('/usuario/permisos', [usuarioController::class, 'permisos'])->name('usuario.permisos');
Route::middleware(['auth:sanctum', 'verified'])->post('/usuario/acceso/{id}', [usuarioController::class, 'acceso'])->name('usuario.acceso');


Route::middleware(['auth:sanctum', 'verified'])->get('/perfil', [usuarioController::class, 'perfil'])->name('perfil');

Route::middleware(['auth:sanctum', 'verified'])->put('/usuarioUpdate/{id}', [usuarioController::class, 'update'])->name('usuarioUpdate');
Route::middleware(['auth:sanctum', 'verified'])->put('/usuarioUpdate/{id}', [usuarioController::class, 'update'])->name('usuarioUpdate');


Route::get('/sucursales', function () {
    return Inertia\Inertia::render('sucursal');
})->name('sucursales');
Route::middleware(['auth:sanctum', 'verified'])->get('/sucursales/all', [sucursalController::class, 'get'])->name('sucursales.get');
Route::middleware(['auth:sanctum', 'verified'])->get('/sucursales/rubros', [sucursalController::class, 'getRubros'])->name('sucursales.rubros');
Route::middleware(['auth:sanctum', 'verified'])->post('/sucursal/save', [sucursalController::class, 'store'])->name('sucursal.store');
Route::middleware(['auth:sanctum', 'verified'])->post('/sucursal/{id}', [sucursalController::class, 'show'])->name('sucursal.get');
Route::middleware(['auth:sanctum', 'verified'])->post('/sucursal/active', [sucursalController::class, 'activar'])->name('sucursal.active');

Route::middleware(['auth:sanctum', 'verified'])->post('/usuario/store', [usuarioController::class, 'update'])->name('usuarioUpdate');

Route::middleware(['auth:sanctum', 'verified'])->get('/perfils', [perfilController::class, 'index'])->name('perfils');
Route::post('/perfils/store', [perfilController::class, 'store'])->name('perfils.store');
Route::post('/perfils/desactivar/{id}', [perfilController::class, 'desactivar'])->name('perfils.desactivar');
Route::post('/perfils/activar/{id}', [perfilController::class, 'activar'])->name('perfils.activar');
Route::get('/perfils/lista', [perfilController::class, 'list'])->name('perfils.lista');
//Route::get('/usuarios', 'usuarioController@index');